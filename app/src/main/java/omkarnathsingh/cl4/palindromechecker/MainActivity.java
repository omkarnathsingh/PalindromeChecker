package omkarnathsingh.cl4.palindromechecker;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final boolean[] textViewAlreadyAdded = {false};
        final TextView textView = new TextView(this);
        final String[] string = new String[1];
        final EditText editText = new EditText(this);
        editText.setId(2);
        editText.setTextSize(25);

        Button button = new Button(this);
        button.setId(1);
        button.setText("Submit");
        button.setBackgroundColor(Color.BLUE);

        RelativeLayout.LayoutParams buttonParams =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        buttonParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        buttonParams.addRule(RelativeLayout.CENTER_VERTICAL);

        final RelativeLayout.LayoutParams editTextParams =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        editTextParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        editTextParams.setMargins(0, 0, 0, 80);
        editTextParams.addRule(RelativeLayout.ABOVE, button.getId());

        final RelativeLayout.LayoutParams textParams =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.setMargins(0, 80, 0, 0);
        textParams.addRule(RelativeLayout.BELOW, button.getId());

        final RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setBackgroundColor(Color.GRAY);
        relativeLayout.addView(editText, editTextParams);
        relativeLayout.addView(button, buttonParams);

        textView.setTextSize(25);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                string[0] = editText.getText().toString();

                if (string[0].equals(new StringBuilder(string[0]).reverse().toString())) {
                    textView.setText(R.string.palindrome);
                } else {
                    textView.setText(R.string.notpalindrome);
                }

                if (!textViewAlreadyAdded[0]) {
                    relativeLayout.addView(textView, textParams);
                    textViewAlreadyAdded[0] = true;
                }
            }
        });

        setContentView(relativeLayout);
    }
}
